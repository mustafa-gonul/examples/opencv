#include <iostream>
#include <opencv2/opencv.hpp>

using namespace cv;

int main(int argc, const char** argv)
{
    const char* exe_name = argv[0];
    if ( argc < 2 ) {
        std::cout << "usage: " << exe_name << " <Image_Path>" << std::endl;
        return -1;
    }

    Mat image = imread(argv[1], 1);
    if (image.empty()) {
        std::cout << "No image data!" << std::endl;
        return -1;
    }

    // converting single channel
    Mat gray;
    cvtColor(image, gray, COLOR_RGB2GRAY);

    // box filter
    Mat filtered;
    boxFilter(gray, filtered, -1, Size(5, 5));

    namedWindow(exe_name, WINDOW_AUTOSIZE);
    do {
        imshow(exe_name, image);
        if (waitKey(0) == 27) // ESC
            break;

        imshow(exe_name, gray);
        if (waitKey(0) == 27) // ESC
            break;

        imshow(exe_name, filtered);
        if (waitKey(0) == 27) // ESC
            break;
    }
    while (true);
    
    return 0;
}