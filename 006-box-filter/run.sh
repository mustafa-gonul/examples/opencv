#!/bin/sh

# Directory and executable has the same name

EXE=${PWD##*/}

./build.sh

cd install || exit

./bin/"$EXE" ./share/lenna.png
# ./bin/"$EXE" ./share/sudoku-original.jpg
