#!/bin/sh

# Directory and executable has the same name

EXE=${PWD##*/}

./build.sh

cd install || exit

./bin/"$EXE" --directory="../share/01_polarized_internal_light/"    --extension=".BMP" --prefix=01_polarized_internal_light_
./bin/"$EXE" --directory="../share/02_unpolarized_internal_light/"  --extension=".BMP" --prefix=02_unpolarized_internal_light_
./bin/"$EXE" --directory="../share/03_without_internal_light/"      --extension=".BMP" --prefix=03_without_internal_light_
./bin/"$EXE" --directory="../share/04_polarized_internal_light_02/" --extension=".BMP" --prefix=04_polarized_internal_light_02_
./bin/"$EXE" --directory="../share/05_far_internal_light/"          --extension=".BMP" --prefix=05_far_internal_light_
./bin/"$EXE" --directory="../share/06_high_sensitivity/"            --extension=".BMP" --prefix=06_high_sensitivity_
# ./bin/"$EXE" --directory="../share/07_auto_exp_segmente/"           --extension=".BMP" --prefix=07_auto_exp_segmente_
./bin/"$EXE" --directory="../share/08_all_leds_angle_60/"           --extension=".BMP" --prefix=08_all_leds_angle_60_
./bin/"$EXE" --directory="../share/09_polarized_angle_80/"          --extension=".BMP" --prefix=09_polarized_angle_80_
