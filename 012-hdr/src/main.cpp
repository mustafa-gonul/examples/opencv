#include <vector>
#include <iostream>
#include <string>
#include <filesystem>
#include <regex>

#include <boost/program_options.hpp>

#include <opencv2/photo.hpp>
#include <opencv2/xphoto/tonemap.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>


using namespace cv;
using namespace std;

namespace po = boost::program_options;
namespace fs = std::filesystem;

void run(vector<Mat> &images, vector<float> &times, const string& prefix)
{
  // Align input images
  cout << "    Aligning images ... " << endl;
  Ptr<AlignMTB> alignMTB = createAlignMTB();
  alignMTB->process(images, images);

  // Obtain Camera Response Function (CRF)
  cout << "    Calculating Camera Response Function (CRF) ... " << endl;
  Mat responseDebevec;
  Ptr<CalibrateDebevec> calibrateDebevec = createCalibrateDebevec();
  calibrateDebevec->process(images, responseDebevec, times);

  // Merge images into an HDR linear image
  cout << "    Merging images into one HDR image ... " << endl;
  Mat hdrDebevec;
  Ptr<MergeDebevec> mergeDebevec = createMergeDebevec();
  mergeDebevec->process(images, hdrDebevec, times, responseDebevec);
  // Save HDR image.
  imwrite(prefix + "-hdrDebevec.hdr", hdrDebevec);

  // Tonemap using Drago's method to obtain 24-bit color image
  cout << "    Tonemaping using Drago's method ... " << endl;
  Mat ldrDrago;
  Ptr<TonemapDrago> tonemapDrago = createTonemapDrago(1.0, 0.7);
  tonemapDrago->process(hdrDebevec, ldrDrago);
  ldrDrago = 3 * ldrDrago;
  imwrite(prefix + "-ldr-Drago.bmp", ldrDrago * 255);

  // Tonemap using Durand's method obtain 24-bit color image
  cout << "    Tonemaping using Durand's method ... " << endl;
  Mat ldrDurand;
  Ptr<xphoto::TonemapDurand> tonemapDurand = xphoto::createTonemapDurand(1.5, 4, 1.0, 1,1);
  tonemapDurand->process(hdrDebevec, ldrDurand);
  ldrDurand = 3 * ldrDurand;
  imwrite(prefix + "-ldr-Durand.bmp", ldrDurand * 255);

  // Tonemap using Reinhard's method to obtain 24-bit color image
  cout << "    Tonemaping using Reinhard's method ... " << endl;
  Mat ldrReinhard;
  Ptr<TonemapReinhard> tonemapReinhard = createTonemapReinhard(2.0, -6, 1, 1); // 1.5, 0, 0, 0
  tonemapReinhard->process(hdrDebevec, ldrReinhard);
  imwrite(prefix + "-ldr-Reinhard.bmp", ldrReinhard * 255);

  // Tonemap using Mantiuk's method to obtain 24-bit color image
  cout << "    Tonemaping using Mantiuk's method ... " << endl;
  Mat ldrMantiuk;
  Ptr<TonemapMantiuk> tonemapMantiuk = createTonemapMantiuk(2.2, 0.85, 1.2);
  tonemapMantiuk->process(hdrDebevec, ldrMantiuk);
  ldrMantiuk = 3 * ldrMantiuk;
  imwrite(prefix + "-ldr-Mantiuk.bmp", ldrMantiuk * 255);
}

template <typename Func, typename... Args>
void traverse(string const& path, string const& extension, Func func, Args&... args)
{
  cout << fs::path(path).relative_path() << endl;

  for (auto& entry : fs::directory_iterator(path)) {
    auto p = entry.path();
    if (p.extension() == extension) {
      auto s = p.string();
      func(p, args...);
    }
  }
}

void add_image_time(string const& path_str, vector<Mat>& images, vector<float>& values)
{
  fs::path p = path_str;
  auto s = p.stem().string();

  regex re{R"(.*_([0-9]+)us_.*)"};
  smatch m;

  images.push_back(imread(fs::absolute(p)));
  if (regex_match(s, m, re)) {
    // images.push_back(imread(fs::absolute(p)));
    values.push_back(stof(m[1]) * 1e-6f); // TODO mustafa
  }
  else {
    values.push_back(5 * 1e-4f); // TODO mustafa
  }

}

int main(int argc, const char** argv)
{


  const char* exe_name = argv[0];
  if ( argc < 2 ) {
    std::cout << "usage: " << exe_name << " <Image_Dir_Path>" << std::endl;
    return -1;
  }

  po::options_description desc("Allowed options");
  desc.add_options()
    ("help",                             "produce help message")
    ("directory,D", po::value<string>(), "directory")
    ("extension,E", po::value<string>(), "extension")
    ("prefix,P",    po::value<string>(), "prefix")
    // ("time,T",      po::value<float>(),  "time")
  ;

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);


  if (vm.count("help")) {
    cout << desc << endl;
    return 1;
  }

  if (vm.count("directory") < 1) {
    cout << "No directory!" << endl;
    return -1;
  }

  if (vm.count("extension") < 1) {
    cout << "No extension!" << endl;
    return -1;
  }

  if (vm.count("prefix") < 1) {
    cout << "No prefix!" << endl;
    return -1;
  }

  auto dir = vm["directory"].as<string>();
  auto ext = vm["extension"].as<string>();
  auto pre = vm["prefix"].as<string>();
  // auto tim = vm["time"].as<float>();


  vector<Mat> images;
  vector<float> times;
  traverse(dir, ext, add_image_time, images, times);
  run(images, times, pre);

  return 0;
}







