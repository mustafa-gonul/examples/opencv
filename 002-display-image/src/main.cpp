#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace cv;

int main(int argc, const char** argv)
{
    const char* exe_name = argv[0];
    if ( argc < 2 ) {
        std::cout << "usage: " << exe_name << " <Image_Path>" << std::endl;
        return -1;
    }

    Mat image = imread(argv[1], 1);
    if (image.empty()) {
        std::cout << "No image data!" << std::endl;
        return -1;
    }

    namedWindow(exe_name, WINDOW_AUTOSIZE);
    imshow(exe_name, image);

    while (waitKey(0) != 27) // ESC
        ;

    return 0;
}
