#include <iostream>

#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

int main(int argc, const char** argv)
{
    const char* exe_name = argv[0];
    if ( argc < 2 ) {
        cout << "usage: " << exe_name << " <Image_Path>" << endl;
        return -1;
    }

    Mat source = imread(argv[1], IMREAD_GRAYSCALE);
    if (source.empty()) {
        cout << "No image data!" << endl;
        return -1;
    }

    /*
     0: Binary
     1: Binary Inverted
     2: Threshold Truncated
     3: Threshold to Zero
     4: Threshold to Zero Inverted
   */
    int threshold_type = 0;
    Mat thresholded;
    threshold(source, thresholded, 240, 255, threshold_type);

    Mat blurred;
    medianBlur(thresholded, blurred, 5);

    Ptr<CLAHE> clahe = createCLAHE();
    clahe->setClipLimit(6);

    Mat destination;
    clahe->apply(source ,destination);

    destination = destination - blurred;

    namedWindow(exe_name, WINDOW_AUTOSIZE);
    do {
        imshow(exe_name, source);
        if (waitKey(0) == 27) // ESC
            break;

        imshow(exe_name, thresholded);
        if (waitKey(0) == 27) // ESC
            break;

        imshow(exe_name, blurred);
        if (waitKey(0) == 27) // ESC
            break;

        imshow(exe_name, destination);
        if (waitKey(0) == 27) // ESC
            break;
    }
    while (true);

    return 0;
}

