# OpenCV Examples

The examples are created for the testing purpose of the OpenCV features and functionality.

## How to Use

- Copy the [001-template](#001-template) example.
- Please delete the **build** and **install** directories if you build once the template example on your side.
- Give a proper index and name.
- Make your changes in the **src/main.cpp** file.
- Run **run.sh** script.

## Example Structure

- **share**: Normally it is an empty directory. When you build the example with **build.sh** or **run.sh**, the images in the main directory will be copied to share directory. If your example needs special images, please add here. Normally images files should be in the **images** directory in the main directory.
- **src/main.cpp**: The main cpp file.
- **build.sh**: One can build the example with the script.
- **clean.sh**: One can clean the example with the script.
- **run.sh**: One can run the example with this script.
- **CMakeLists.txt**: CMake file for building the example. If you are using only **main.cpp** file for the example, you don't need to update file. It is ready to use. If you have several source files and headers files you need to add them.

## Filters

I have created this section because most of the time I have a problem with understanding and explaining the OpenCV filter names to other people in the project.

| OpenCV Name | Well Known Explanation |
|-------------|------------------------|
| boxFilter   | Mean filter            |
| dilate      | Max filter             |
| erode       | Min filter             |
| medianBlur  | Median filter          |
| pyrDown     | Down Sampling          |
| pyrUp       | Up sampling            |
| sober       | Sober                  |

## TODO

- After build and run the example you should clean the example.
- You can call the clean script in the build script.
- Create examples to test other functions in the **imgproc** module.

## Examples

### 001-template

If you want to create an example, you can start with copying this example as a boilerplate.

### 002-display-image

The example is to test the GUI features of OpenCV.

**References:**

- [Image file reading and writing](https://docs.opencv.org/master/d4/da8/group__imgcodecs.html)
- [High-level GUI](https://docs.opencv.org/master/d7/dfc/group__highgui.html)

### 003-print-image-properties

In the example, an auxiliary function is developed to print **Mat** properties.

**References:**

- [cv::Mat Class Reference](https://docs.opencv.org/master/d3/d63/classcv_1_1Mat.html)
- [How to find out what type of a Mat object is with Mat::type() in OpenCV
](https://stackoverflow.com/questions/10167534/how-to-find-out-what-type-of-a-mat-object-is-with-mattype-in-opencv)

### 004-convert-image-gray

The color (3 channel) image is converted to gray (1 channel) one.

**References:**

- [Color Space Conversions](https://docs.opencv.org/master/d8/d01/group__imgproc__color__conversions.html)

### 005-threshold-mat

**!!! Empty !!!**

### 006-box-filter

The example is created to test the **box filter**. The other name of the filter is **mean filter**.

**References:**

- [Image Filtering](https://docs.opencv.org/master/d4/d86/group__imgproc__filter.html)

### 007-median-blur

The example is created to test **median blur** The other name of the filter is **median filter** only.

**References:**

- [Image Filtering](https://docs.opencv.org/master/d4/d86/group__imgproc__filter.html)

### 008-pyr-down

The example is created to test **pyramid down**. The other name of the filter is **down sampling filter**.

**References:**

- [Image Filtering](https://docs.opencv.org/master/d4/d86/group__imgproc__filter.html)

### 009-pyr-up

The example is created to test **pyramid up**. The other name of the filter is **up sampling filter**.

**References:**

- [Image Filtering](https://docs.opencv.org/master/d4/d86/group__imgproc__filter.html)

### 010-erode

The example is created to test **erode**. The other name of the filter is **min filter**.

**References:**

- [Image Filtering](https://docs.opencv.org/master/d4/d86/group__imgproc__filter.html)

### 011-dilate

The example is created to test **dilate**. The other name of the filter is **max filter**.

**References:**

- [Image Filtering](https://docs.opencv.org/master/d4/d86/group__imgproc__filter.html)

### 012-hdr

The example is based on the [High Dynamic Range (HDR) Imaging using OpenCV (C++/Python)](https://www.learnopencv.com/high-dynamic-range-hdr-imaging-using-opencv-cpp-python/). Thanks to [Satya Mallick](https://www.linkedin.com/in/satyamallick/).

**References:**

-[High Dynamic Range (HDR) Imaging using OpenCV (C++/Python)](https://www.learnopencv.com/high-dynamic-range-hdr-imaging-using-opencv-cpp-python/)

### 013-clahe

Histogram equalization.

**References:**

- [Histograms - 2: Histogram Equalization](https://docs.opencv.org/master/d5/daf/tutorial_py_histogram_equalization.html)

### 014-remove-glare

In the example, the glare tried to be removed from the image. The entries in the references is considered as a base.

**References:**

- [Preprocessing for Automatic Pattern Identification in Wildlife: Removing Glare](http://www.amphident.de/en/blog/preprocessing-for-automatic-pattern-identification-in-wildlife-removing-glare.html)
- [How to remove the glare and brightness in an image (Image preprocessing)?](https://dsp.stackexchange.com/questions/1215/how-to-remove-the-glare-and-brightness-in-an-image-image-preprocessing)
- [Remove glare from photo opencv](https://stackoverflow.com/questions/43470569/remove-glare-from-photo-opencv)

### 015-hdr-merge**References:**

TBD

### 016-merge

TBD
