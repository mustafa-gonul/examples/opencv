cmake_minimum_required(VERSION 3.0.0)

# Enable C++17
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED TRUE)

# Get Directory Name
get_filename_component(executable ${CMAKE_CURRENT_SOURCE_DIR} NAME)

# Project
project(${executable} VERSION 0.1.0)

# OpenCV
find_package(OpenCV REQUIRED)

# OpenCV Status
message(STATUS "OpenCV library status:")
message(STATUS "    config: ${OpenCV_DIR}")
message(STATUS "    version: ${OpenCV_VERSION}")
message(STATUS "    libraries: ${OpenCV_LIBS}")
message(STATUS "    include path: ${OpenCV_INCLUDE_DIRS}")

# Variables
set(executable ${PROJECT_NAME})

# Directories
set(project_BIN_DIR bin)
set(project_INC_DIR include)
set(project_LIB_DIR lib)
set(project_SHR_DIR share)
set(project_SRC_DIR src)

# Project files
set(project_SOURCES ${project_SRC_DIR}/main.cpp)
file(GLOB project_SHARES "${project_SHR_DIR}/*")
list(FILTER project_SHARES INCLUDE REGEX ".(png|jpg)$")

# Add executable to project
add_executable(${executable} ${project_SOURCES})

# Linking
target_link_libraries(${executable} PRIVATE ${OpenCV_LIBS})

# Install
install(TARGETS ${executable}     DESTINATION ${project_BIN_DIR})
install(FILES   ${project_SHARES} DESTINATION ${project_SHR_DIR})

