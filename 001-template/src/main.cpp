#include <iostream>

int main(int argc, const char** argv) 
{
    if (argc < 2) {
        std::cout << "No Arguments!" << std::endl;
    }
    else {
        std::cout << "Arg: " << argv[1] << std::endl;
    }
        
    return 0;
}
