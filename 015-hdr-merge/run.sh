#!/bin/sh

# Directory and executable has the same name

EXE=${PWD##*/}

./build.sh

cd install || exit

./bin/"$EXE" --prefix=01_ \
  "../share/07_auto_exp_segmente/MainBeam_AutoExp_Hs=Off_01.BMP" \
  "../share/07_auto_exp_segmente/polarized_AutoExp_Hs=Off_01.BMP" \
  "../share/07_auto_exp_segmente/unpolarizied_AutoExp_Hs=Off_01.BMP"

./bin/"$EXE" --prefix=02_ \
  "../share/07_auto_exp_segmente/MainBeam_AutoExp_Hs=Off_02.BMP" \
  "../share/07_auto_exp_segmente/polarized_AutoExp_Hs=Off_02.BMP" \
  "../share/07_auto_exp_segmente/unpolarizied_AutoExp_Hs=Off_02.BMP"

./bin/"$EXE" --prefix=03_ \
  "../share/07_auto_exp_segmente/MainBeam_AutoExp_Hs=Off_03.BMP" \
  "../share/07_auto_exp_segmente/polarized_AutoExp_Hs=Off_03.BMP" \
  "../share/07_auto_exp_segmente/unpolarizied_AutoExp_Hs=Off_03.BMP"

./bin/"$EXE" --prefix=04_ \
  "../share/07_auto_exp_segmente/MainBeam_AutoExp_Hs=Off_04.BMP" \
  "../share/07_auto_exp_segmente/polarized_AutoExp_Hs=Off_04.BMP" \
  "../share/07_auto_exp_segmente/unpolarizied_AutoExp_Hs=Off_04.BMP"

./bin/"$EXE" --prefix=05_ \
  "../share/07_auto_exp_segmente/MainBeam_AutoExp_Hs=Off_05.BMP" \
  "../share/07_auto_exp_segmente/polarized_AutoExp_Hs=Off_05.BMP" \
  "../share/07_auto_exp_segmente/unpolarizied_AutoExp_Hs=Off_05.BMP"
