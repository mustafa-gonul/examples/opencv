#include <opencv2/photo.hpp>
#include <opencv2/xphoto/tonemap.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>

#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <filesystem>

#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>


using namespace cv;
using namespace std;

namespace fs = std::filesystem;


void run(vector<Mat> &images, vector<float> &times, const string& prefix)
{
  // Align input images
  cout << "Aligning images ... " << endl;
  Ptr<AlignMTB> alignMTB = createAlignMTB();
  alignMTB->process(images, images);

  // Obtain Camera Response Function (CRF)
  cout << "Calculating Camera Response Function (CRF) ... " << endl;
  Mat responseDebevec;
  Ptr<CalibrateDebevec> calibrateDebevec = createCalibrateDebevec();
  calibrateDebevec->process(images, responseDebevec, times);

  // Merge images into an HDR linear image
  cout << "Merging images into one HDR image ... " ;
  Mat hdrDebevec;
  Ptr<MergeDebevec> mergeDebevec = createMergeDebevec();
  mergeDebevec->process(images, hdrDebevec, times, responseDebevec);
  // Save HDR image.
  imwrite(prefix + "-hdrDebevec.hdr", hdrDebevec);
  cout << "saved hdrDebevec.hdr "<< endl;

  // Tonemap using Drago's method to obtain 24-bit color image
  cout << "Tonemaping using Drago's method ... ";
  Mat ldrDrago;
  Ptr<TonemapDrago> tonemapDrago = createTonemapDrago(1.0, 0.7);
  tonemapDrago->process(hdrDebevec, ldrDrago);
  ldrDrago = 3 * ldrDrago;
  imwrite(prefix + "-ldr-Drago.bmp", ldrDrago * 255);
  cout << "saved ldr-Drago.bmp"<< endl;

  // Tonemap using Durand's method obtain 24-bit color image
  cout << "Tonemaping using Durand's method ... ";
  Mat ldrDurand;
  Ptr<xphoto::TonemapDurand> tonemapDurand = xphoto::createTonemapDurand(1.5, 4, 1.0, 1,1);
  tonemapDurand->process(hdrDebevec, ldrDurand);
  ldrDurand = 3 * ldrDurand;
  imwrite(prefix + "-ldr-Durand.bmp", ldrDurand * 255);
  cout << "saved ldr-Durand.bmp"<< endl;

  // Tonemap using Reinhard's method to obtain 24-bit color image
  cout << "Tonemaping using Reinhard's method ... ";
  Mat ldrReinhard;
  Ptr<TonemapReinhard> tonemapReinhard = createTonemapReinhard(1.8, -8, 1, 1); // 1.5, 0, 0, 0
  tonemapReinhard->process(hdrDebevec, ldrReinhard);
  imwrite(prefix + "-ldr-Reinhard.bmp", ldrReinhard * 220);
  cout << "saved ldr-Reinhard.bmp"<< endl;

  // Tonemap using Mantiuk's method to obtain 24-bit color image
  cout << "Tonemaping using Mantiuk's method ... ";
  Mat ldrMantiuk;
  Ptr<TonemapMantiuk> tonemapMantiuk = createTonemapMantiuk(2.2, 0.85, 1.2);
  tonemapMantiuk->process(hdrDebevec, ldrMantiuk);
  ldrMantiuk = 3 * ldrMantiuk;
  imwrite(prefix + "-ldr-Mantiuk.bmp", ldrMantiuk * 255);
  cout << "saved ldr-Mantiuk.bmp"<< endl;
}

void createImagesTimes(vector<string>const& files, float default_time, vector<Mat>& images, vector<float>& times)
{
  for (auto const& file : files) {
    Mat im = imread(fs::absolute(file));

    images.push_back(im);
    times.push_back(default_time);
  }
}

int main(int argc, const char** argv)
{
  namespace po = boost::program_options;

  const char* exe_name = argv[0];
  if ( argc < 2 ) {
    std::cout << "usage: " << exe_name << " <Image_Dir_Path>" << std::endl;
    return -1;
  }

  constexpr float default_shutter_time = 0.0005;
  float shutter_time = default_shutter_time;
  po::options_description desc("Allowed options");
  desc.add_options()
    ("help",                                                                           "Produce help message")
    ("prefix,P", po::value<string>(),                                                  "Prefix for output file")
    ("time,T",   po::value<float>(&shutter_time)->default_value(default_shutter_time), "Shutter time")
    ("file",     po::value<vector<string>>(),                                          "Image files")
  ;

  po::positional_options_description p;
  p.add("file", -1);

  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
  po::notify(vm);

  if (vm.count("help")) {
    cout << desc << endl;
    return 1;
  }

  if (vm.count("file") < 1) {
    cout << "No file!" << endl;
    return -1;
  }

  if (vm.count("prefix") < 1) {
    cout << "No prefix!" << endl;
    return -1;
  }

  auto files  = vm["file"].as<vector<string>>();
  auto prefix = vm["prefix"].as<string>();


  cout << "Files:" << endl;
  for (auto& f : files) {
    cout << "    -> " << f << endl;
  }
  cout << "Prefix:" << prefix << endl;
  cout << "Shutter time:" << shutter_time << endl;


  vector<Mat> images;
  vector<float> times;
  createImagesTimes(files, default_shutter_time, images, times);
  run(images, times, prefix);

  return 0;
}







