#!/bin/sh

cd "$(dirname "$0")" || exit

cp ../images/* ./share

rm -Rf build
rm -Rf install

mkdir build
mkdir install

cd build || exit

# Default: /usr/local
# cmake ..

# Custom: ../install
cmake .. -DCMAKE_INSTALL_PREFIX=../install

## Verbose on
# cmake .. -DCMAKE_INSTALL_PREFIX=../install -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON
# make VERBOSE=1

make
make install