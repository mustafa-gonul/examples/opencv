#include <stdio.h>
#include <opencv2/opencv.hpp>


using namespace std;
using namespace cv;

//
// Reference: https://stackoverflow.com/questions/10167534/how-to-find-out-what-type-of-a-mat-object-is-with-mattype-in-opencv
//
std::string type2str(int type) {
  std::string r;

  uchar depth = type & CV_MAT_DEPTH_MASK;
  uchar chans = 1 + (type >> CV_CN_SHIFT);

  switch ( depth ) {
    case CV_8U:  r = "8U"; break;
    case CV_8S:  r = "8S"; break;
    case CV_16U: r = "16U"; break;
    case CV_16S: r = "16S"; break;
    case CV_32S: r = "32S"; break;
    case CV_32F: r = "32F"; break;
    case CV_64F: r = "64F"; break;
    default:     r = "User"; break;
  }

  r += "C";
  r += (chans+'0');

  return r;
}

std::string type2str(Mat const& image) {
    return type2str(image.type());
}

void print_mat(cv::Mat const& image) {
    std::cout << "Size:     " << image.size       << std::endl;
    std::cout << "Rows:     " << image.rows       << std::endl;
    std::cout << "Cols:     " << image.cols       << std::endl;
    std::cout << "Step:     " << image.step       << std::endl;
    std::cout << "Flag:     " << image.flags      << std::endl;
    std::cout << "Type:     " << image.type()     << std::endl;
    std::cout << "Type Str: " << type2str(image)  << std::endl;
    std::cout << "Depth:    " << image.depth()    << std::endl;
    std::cout << "Channels: " << image.channels() << std::endl;
}

int main(int argc, const char** argv)
{
    string exe_name = argv[0];
    if ( argc < 2 ) {
        std::cout << "usage: " << exe_name << " <Image_Path>" << std::endl;
        return -1;
    }

    Mat image = imread(argv[1], 1);
    if (image.empty()) {
        std::cout << "No image data!" << std::endl;
        return -1;
    }

    Mat hsv;
    cvtColor(image, hsv, COLOR_RGB2HLS);

    cout << "---------- RGB ------------- " << endl;
    print_mat(image);
    cout << "---------- HSV ------------- " << endl;
    print_mat(hsv);

    vector<Mat> hsvChannels;
    split(hsv, hsvChannels);
    auto o = hsvChannels[1];
    hsvChannels.clear();

    // Original
    auto o_name = exe_name + " - original";
    namedWindow(o_name, WINDOW_AUTOSIZE);
    imshow(o_name, o);

    // Saturation
    auto s = o > 200;
    auto s_name = exe_name + " - saturation";
    namedWindow(s_name, WINDOW_AUTOSIZE);
    imshow(s_name, s);

    // Dilate
    Mat e;
    dilate(s, e, getStructuringElement(MORPH_ELLIPSE, Size(3, 3)));

    auto e_name = exe_name + " - erode";
    namedWindow(e_name, WINDOW_AUTOSIZE);
    imshow(e_name, e);

    // Inpaint
    Mat i;
    inpaint(o, e, i, 11, INPAINT_TELEA);
    auto i_name = exe_name + " - inpaint";
    namedWindow(i_name, WINDOW_AUTOSIZE);
    imshow(i_name, i);


    /*
    // Result
    auto r = o - s / 2;

    auto r_name = exe_name + " - result";
    namedWindow(r_name, WINDOW_AUTOSIZE);
    imshow(r_name, r);

    // clahe
    Ptr<CLAHE> clahe = createCLAHE();
    clahe->setClipLimit(6);
    Mat c;
    clahe->apply(r ,c);

    auto c_name = exe_name + " - clahe";
    namedWindow(c_name, WINDOW_AUTOSIZE);
    imshow(c_name, r);
    */


    waitKey(0);




    /*
    do {
        imshow(exe_name, image);
        if (waitKey(0) == 27) // ESC
            break;

        imshow(exe_name, gray);
        if (waitKey(0) == 27) // ESC
            break;
    }
    while (true);
    */

    return 0;
}
