#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace cv;

// 
// Reference: https://stackoverflow.com/questions/10167534/how-to-find-out-what-type-of-a-mat-object-is-with-mattype-in-opencv
//
std::string type2str(int type) {
  std::string r;

  uchar depth = type & CV_MAT_DEPTH_MASK;
  uchar chans = 1 + (type >> CV_CN_SHIFT);

  switch ( depth ) {
    case CV_8U:  r = "8U"; break;
    case CV_8S:  r = "8S"; break;
    case CV_16U: r = "16U"; break;
    case CV_16S: r = "16S"; break;
    case CV_32S: r = "32S"; break;
    case CV_32F: r = "32F"; break;
    case CV_64F: r = "64F"; break;
    default:     r = "User"; break;
  }

  r += "C";
  r += (chans+'0');

  return r;
}

std::string type2str(Mat const& image) {
    return type2str(image.type());
}

void print_mat(cv::Mat const& image) {
    std::cout << "Size:     " << image.size       << std::endl;
    std::cout << "Rows:     " << image.rows       << std::endl;
    std::cout << "Cols:     " << image.cols       << std::endl;
    std::cout << "Step:     " << image.step       << std::endl;
    std::cout << "Flag:     " << image.flags      << std::endl;
    std::cout << "Type:     " << image.type()     << std::endl;
    std::cout << "Type Str: " << type2str(image)  << std::endl;
    std::cout << "Depth:    " << image.depth()    << std::endl;
    std::cout << "Channels: " << image.channels() << std::endl;
}

int main(int argc, const char** argv)
{
    const char* exe_name = argv[0];
    if ( argc < 2 ) {
        std::cout << "usage: " << exe_name << " <Image_Path>" << std::endl;
        return -1;
    }

    Mat image = imread(argv[1], 1);
    if (image.empty()) {
        std::cout << "No image data!" << std::endl;
        return -1;
    }

    Mat gray;
    cvtColor(image, gray, COLOR_RGB2GRAY);

    std::cout << "--------- Original --------- " << std::endl;
    print_mat(image);
    std::cout << "--------- Gray ------------- " << std::endl;
    print_mat(gray);

    namedWindow(exe_name, WINDOW_AUTOSIZE);
    do {
        imshow(exe_name, image);
        if (waitKey(0) == 27) // ESC
            break;

        imshow(exe_name, gray);
        if (waitKey(0) == 27) // ESC
            break;
    }
    while (true);
    
    return 0;
}
