#!/bin/sh

# Directory and executable has the same name

EXE=${PWD##*/}

./build.sh

cd install || exit

./bin/"$EXE" \
  --output output_01.bmp \
  --high   "../share/06_high_sensitivity/polarized_03000us_Exp150_Hs=Off_01.BMP" \
  --low    "../share/06_high_sensitivity/polarized_03000us_Exp150_Hs=On_01.BMP"  \
