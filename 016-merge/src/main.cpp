#include <iostream>
#include <boost/program_options.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>

using namespace std;
using namespace cv;

namespace po = boost::program_options;

//
// Reference: https://stackoverflow.com/questions/10167534/how-to-find-out-what-type-of-a-mat-object-is-with-mattype-in-opencv
//
string type2str(int type) {
  string r;

  uchar depth = type & CV_MAT_DEPTH_MASK;
  uchar chans = 1 + (type >> CV_CN_SHIFT);

  switch ( depth ) {
    case CV_8U:  r = "8U"; break;
    case CV_8S:  r = "8S"; break;
    case CV_16U: r = "16U"; break;
    case CV_16S: r = "16S"; break;
    case CV_32S: r = "32S"; break;
    case CV_32F: r = "32F"; break;
    case CV_64F: r = "64F"; break;
    default:     r = "User"; break;
  }

  r += "C";
  r += (chans + '0');

  return r;
}

string type2str(Mat const& image) {
    return type2str(image.type());
}

void print_mat(cv::Mat const& image) {
    cout << "Size:     " << image.size       << endl;
    cout << "Rows:     " << image.rows       << endl;
    cout << "Cols:     " << image.cols       << endl;
    cout << "Step:     " << image.step       << endl;
    cout << "Flag:     " << image.flags      << endl;
    cout << "Type:     " << image.type()     << endl;
    cout << "Type Str: " << type2str(image)  << endl;
    cout << "Depth:    " << image.depth()    << endl;
    cout << "Channels: " << image.channels() << endl;
}

int main(int argc, const char** argv)
{
  constexpr float default_shutter_time = 0.0005;
  float shutter_time = default_shutter_time;

  po::options_description desc("Allowed options");
  desc.add_options()
    ("help",                                                                           "Produce help message")
    ("output,O", po::value<string>(),                                                  "Output file name")
    ("time,T",   po::value<float>(&shutter_time)->default_value(default_shutter_time), "Shutter time")
    ("high,H",   po::value<string>(),                                                  "High Sensitivity Image")
    ("low,L",    po::value<string>(),                                                  "Low Sensitivity Image")
  ;

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help")) {
    cout << desc << endl;
    return 1;
  }

  if (vm.count("output") < 1) {
    cout << "No output!" << endl;
    return 1;
  }

  if (vm.count("high") < 1) {
    cout << "No high!" << endl;
    return 1;
  }

  if (vm.count("low") < 1) {
    cout << "No low!" << endl;
    return 1;
  }


  auto output_str  = vm["output"].as<string>();
  auto high_str = vm["high"].as<string>();
  auto low_str  = vm["low"].as<string>();

  cout << "Output:" << output_str << endl;
  cout << "High:" << high_str << endl;
  cout << "Low:" << low_str << endl;
  cout << "Shutter time:" << shutter_time << endl;

  auto im_low  = imread(low_str, IMREAD_UNCHANGED);
  auto im_high = imread(high_str, IMREAD_UNCHANGED);
  assert(im_low.size == im_high.size);

  cout << "-------------------   Low   --------------------------------" << endl;
  print_mat(im_low);
  cout << "-------------------   High  --------------------------------" << endl;
  print_mat(im_high);

  cout << "-------------------  Result --------------------------------" << endl;
  Mat result(im_low.rows, im_low.cols, CV_16UC1);
  print_mat(result);

  // auto r_iter = result.begin<ushort>();
  // auto r_end =  result.end<ushort>();

  auto r_iter = result.begin<ushort>();
  auto r_end =  result.end<ushort>();
  auto l_iter = im_low.begin<uchar>();
  auto h_iter = im_high.begin<uchar>();

  while (r_iter != r_end) {
    auto low = static_cast<ushort>(*l_iter);
    auto high = static_cast<ushort>(*h_iter);

    //auto val = high << 2 | low;
    auto val = (low << 4 | high) << 4;

    *r_iter = val;

    ++r_iter;
    ++l_iter;
    ++h_iter;
  }

  // Ptr<CLAHE> clahe = createCLAHE();
  // clahe->setClipLimit(6);

  // Mat destination;
  // clahe->apply(result ,destination);



  namedWindow("low", WINDOW_AUTOSIZE);
  imshow("low", im_low);
  namedWindow("high", WINDOW_AUTOSIZE);
  imshow("high", im_high);
  namedWindow("result", WINDOW_AUTOSIZE);
  imshow("result", result);
  // namedWindow("destination", WINDOW_AUTOSIZE);
  // imshow("destination", destination);

  while (waitKey(0) != 27) // ESC
      ;

  /*
  vector<Mat> images;
  vector<float> times;
  createImagesTimes(files, default_shutter_time, images, times);
  run(images, times, prefix);
  */

  return 0;
}

